<?php

$table_prefix = 'wp_';

// Copied parts from the wp_get_environment_type() function, since it's not available at this point
$current_env = 'production';

if ( defined( 'WP_ENVIRONMENT_TYPE' ) ) {
	$current_env = WP_ENVIRONMENT_TYPE;
}


if ($current_env === 'local' || $current_env === 'development') {
	define('DB_NAME', 'local');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'root');

	define('WP_DISABLE_FATAL_ERROR_HANDLER', true);
	define('WP_DEBUG', true );
	define('WP_DEBUG_DISPLAY', true);
	ini_set('display_errors', 'Off');
	ini_set('error_reporting', E_ALL);

	define('WP_ALLOW_REPAIR', true);
	define('ALLOW_UNFILTERED_UPLOADS', true);
} else {
	define('DB_NAME', 'xxxxxxxxxxxxxx');
	define('DB_USER', 'xxxxxxxxxxxxxx');
	define('DB_PASSWORD', 'xxxxxxxxxxxxxx');

	define('WP_DEBUG', false );
	define('WP_DEBUG_DISPLAY', false);
	ini_set('display_errors', 'Off');
	ini_set('error_reporting', 0);

	define('AUTOMATIC_UPDATER_DISABLED', true);
	define('WP_AUTO_UPDATE_CORE', false);

	define('DISALLOW_FILE_EDIT', true );
	define('DISALLOW_FILE_MODS', true );
	define('WP_CACHE', true);
}

define( 'DB_HOST', 'localhost' );
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );

/** Memory Limit */
define('WP_MEMORY_LIMIT', '256M');
define( 'WP_MAX_MEMORY_LIMIT', '256M' );

define('AUTH_KEY',         'jTrmz)P-Ah+5l_o0]x7FU_`i+vb+f1e#]R]94IdG$)(r]ONi51trU};R}0yJicM:');
define('SECURE_AUTH_KEY',  'yrVfiy2=i-|~D6?6,6!vW- 08N<o1anEEL4uN S4I^ -VA[v^UhQQ~MA?3fHg?)5');
define('LOGGED_IN_KEY',    'RHz)IyAkQ]0i=F& 2qBj[Lz2Sx|lHZ!,k`*zpt7<+btOA<w|=L*1NUO@ m|qkT&K');
define('NONCE_KEY',        'u.iEQV(`4LqYY,&SBOqR`^c=+6rTk#RthNZLLxLe- |+_#8ry!k.~74Mg5cXlx.-');
define('AUTH_SALT',        '0~1%]/jw4byw5$=)h`|?_0)Ak`*nx6mq 4y<D+~m31S@jWQDtdD%fNM]7D#|Sv[ ');
define('SECURE_AUTH_SALT', '!z[TH:coS+qT,mE8)FuAi0,q]G*,P2hf*3GMy,)V)f{^@&m|>vTifKA785>D*F|6');
define('LOGGED_IN_SALT',   'e)=yw:{=d;<wh]$(X-}>%z.2&`4KV; XcLlSIY_+)]jMCkw8X=jF;^S1x3ylvf*p');
define('NONCE_SALT',       ' 7mt]]#_XSqHHs;fIJ<@DF/d9_u4:jf}zrD[>of[!n]|UL:]F!wk`nM4$:NCI PB');




/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';