<?php

/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

function hello_elementor_child_enqueue_scripts() {
    wp_enqueue_style(
        'hello-elementor-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        ['hello-elementor-theme-style'],
        '1.0.0'
    );
}
add_action('wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts', 20);


/*------------------------------------------------------- ADD WP FUNCTIONALITY ---------------------------------------------------------------*/

// Customize login logo 
function aw_custom_login_logo()
{
	$logo = get_option("site_logo", 0);

	if ($logo != "0"){
		$parsed = parse_url(wp_get_attachment_url($logo) );
		$url = dirname( $parsed [ 'path' ] ) . '/' . rawurlencode( basename( $parsed[ 'path' ] ) );
		echo 
		"<style type='text/css'>
		body.login h1 a { 
			background-image: url(" . $url . "); 
			background-position: center;
			background-size: inherit;
			width:75% !important;
			margin-bottom: 0px
		</style>";
	}
}
add_action('login_head', 'aw_custom_login_logo');

// Adjust link of login logo
function login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'login_logo_url' );


function add_additional_meta()
{?>
<meta content="always" name="referrer" />
<meta content="yes" name="mobile-web-app-capable" />
<meta content="yes" name="apple-mobile-web-app-capable" />
<?php
}
add_action('wp_head', 'add_additional_meta');



/*-------------------------------------------------------------- REMOVALS -----------------------------------------------------------------*/

// Remove WP Emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'rsd_link') ;
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'wp_generator') ;
remove_action('wp_head', 'wlwmanifest_link' ) ;
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);


function disable_embed(){
  wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'disable_embed' );


function disable_pingback( &$links ) {
	foreach ( $links as $l => $link ) {
 		if ( 0 === strpos( $link, get_option( 'home' ) ) ){
 			unset($links[$l]);
 		}
	}
}
add_action( 'pre_ping', 'disable_pingback' );


function wpdocs_dequeue_dashicon() {
	if (current_user_can( 'update_core' )) {
		return;
	}
	wp_deregister_style('dashicons');
}
add_action( 'wp_enqueue_scripts', 'wpdocs_dequeue_dashicon' );


// Remove jQuery migrate
function remove_jquery_migrate($scripts){
	if (!is_admin() && isset($scripts->registered['jquery'])) {
		$script = $scripts->registered['jquery'];
		if ($script->deps) {
			// Check whether the script has any dependencies
			$script->deps = array_diff($script->deps, array('jquery-migrate'));
		}
	}
}
add_action('wp_default_scripts', 'remove_jquery_migrate');


// Removing WordPress version number from the header and RSS
function remove_wordpress_version(){
	return '';
}
add_filter('the_generator', 'remove_wordpress_version');


// Pick out the version number from scripts and styles
function remove_version_from_style_js($src){
	if (strpos($src, 'ver=' . get_bloginfo('version'))){
		$src = remove_query_arg('ver', $src);
	}
	return $src;
}
add_filter('style_loader_src', 'remove_version_from_style_js');
add_filter('script_loader_src', 'remove_version_from_style_js');


// Add SVG to allowed file uploads
function add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes);

	return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');



/*------------------------------------------------------- REMOVE COMMENTS ---------------------------------------------------------------*/
		
add_action('admin_init', function () {
	// Redirect any user trying to access comments page
	global $pagenow;

	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url());
		exit;
	}

	// Remove comments metabox from dashboard
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

	// Disable support for comments and trackbacks in post types
	foreach (get_post_types() as $post_type) {
		if (post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
});


// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);


// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);


// Remove comments page in menu
add_action('admin_menu', function () {
	remove_menu_page('edit-comments.php');
});


// Remove comments links from admin bar
add_action('init', function () {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
});


// Removes comments from admin bar
function mytheme_admin_bar_render()
{
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('comments');
}
add_action('wp_before_admin_bar_render', 'mytheme_admin_bar_render');


?>