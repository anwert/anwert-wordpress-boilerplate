=== Hello Elementor Child Adjusted by Anwert ===

Contributors: Anwert
Requires at least: WordPress 4.7
Tested up to: WordPress 5.2
Stable tag: 1.0.0
Version: 1.0
Requires PHP: 5.4
License: GNU General Public License v3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Tags: flexible-header, custom-colors, custom-menu, custom-logo, editor-style, featured-images, rtl-language-support, threaded-comments, translation-ready

A theme based on the Hello Elementor Child Theme.

***Hello Elementor*** is distributed under the terms of the GNU GPL v3 or later.

== Description ==

The Hello Elementor Child Theme is a starter blank child theme for [Hello Elementor](https://wordpress.org/themes/hello-elementor/) theme.